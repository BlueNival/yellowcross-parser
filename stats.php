<?php
include "simple_html_dom.php";
include "config.php";
include "common.php";

function get_all_posts_of_last_N_days($number) {
  $link = db_open();
  $time = strtotime("midnight -$number days");
  $query = "SELECT * FROM posts WHERE created > $time";
  $result = mysql_query($query) or die("Query failed : " . mysql_error());
  $posts = array();
  while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
    $posts[] = $line;
  }
  db_close($link);
  return $posts;
}

function get_top_N_by_posts_for_time($n, $time) {
    $link = db_open();
    $query = "SELECT authorname, authorid, count(*) as posts FROM posts WHERE created > $time GROUP BY authorid ORDER BY posts DESC LIMIT $n";
    $result = mysql_query($query) or die("Query failed : " . mysql_error());
    $posts = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $posts[] = $line;
    }
    db_close($link);
    return $posts;
}

function user_pages() {
  $pages = array();
  $pages[] = FORUM."/userlist.php";
  $html = file_get_html($pages[0]);
  if (is_array($links = $html->find(".linksb .pagelink a"))) {
    foreach ($links as $link) {
      if (strpos($link->class, 'next') === false) {
        $pages[] = FORUM."/userlist.php?p=".$link->plaintext;
      }
    }
  }
  return $pages;
}

function get_all_users() {
  $pages = user_pages();
  $users = array();
  foreach ($pages as $page) {
    $html = file_get_html($page);
    foreach ($html->find(".usertable tr") as $tr) {
      if ($tr->find(".tc3", 3)->plaintext == "Был") {
        continue;
      }
      $link = $tr->find(".usersname a", 0);
      $time = trim($tr->find(".tc3",3)->plaintext);
      if ($time == "Сегодня") {
        $time = date("Y-m-d", time());
      }
      if ($time == "Вчера") {
        $time = date("Y-m-d", strtotime("Yesterday"));
      }
      $tmp = new stdClass();
      $link_href_array = explode("=", $link->href);
      $tmp->name = $link->plaintext;
      $tmp->id = $link_href_array[1];
      $tmp->time = $time;
      $users[] = $tmp;
    }
  }
  return $users;
}

function absent_for_N_days($n) {
  $users = get_all_users();
  $absent = array();
  $critical_point = strtotime("midnight -$n days");
  foreach ($users as $user) {
    $time = strtotime($user->time." 00:00:00");
    if ($time < $critical_point and $user->id != 2 and $user->id != 8) {
      $absent[] = $user;
    }
  }
  return $absent;
}

date_default_timezone_set('Europe/London');
$posts = get_all_posts_of_last_N_days(7);
if (!date("w", time())) {
  $time = strtotime("midnight monday -1 week");
} else {
    $time = strtotime("midnight monday this week");
}
$top_week = get_top_N_by_posts_for_time(10, $time);
$time = strtotime("midnight first day of this month");
$top_month = get_top_N_by_posts_for_time(10, $time);
$absent = absent_for_N_days(14);
?>

<html>
<head>
  <meta charset="UTF-8">
</head>
<body>
<table>
  <caption>Посты за сегодня</caption>
  <thead>
  <tr>
    <th>Имя пользователя</th>
    <th>Ссылка на пост</th>
    <th>Дата и время</th>
    <th>Раздел</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($posts as $post) : ?>
    <tr>
      <td><a href='<?php print user_url($post["authorid"]); ?>'><?php print $post["authorname"]; ?></a></td>
      <td><a href='<?php print $post["postlink"]; ?>'><?php print $post["themename"]; ?></a></td>
      <td><?php print date("d.m.Y H:i:s", (int)$post["created"]); ?></td>
      <td><?php print $post["forum"]; ?></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
  </table>

<table>
    <caption>Лучшие за неделю</caption>
    <thead>
    <tr>
        <th>Имя пользователя</th>
        <th>ID пользователя</th>
        <th>Количество постов</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($top_week as $row) : ?>
        <tr>
            <td><a href='<?php print user_url($row["authorid"]); ?>'><?php print $row["authorname"]; ?></a></td>
            <td><?php print $row["authorid"]; ?></td>
            <td><?php print $row["posts"] ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<table>
    <caption>Лучшие за месяц</caption>
    <thead>
    <tr>
        <th>Имя пользователя</th>
        <th>ID пользователя</th>
        <th>Количество постов</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($top_month as $row) : ?>
        <tr>
            <td><a href='<?php print user_url($row["authorid"]); ?>'><?php print $row["authorname"]; ?></a></td>
            <td><?php print $row["authorid"]; ?></td>
            <td><?php print $row["posts"] ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<table>
  <caption>Отсутствуют больше 14 дней</caption>
  <thead>
  <tr>
    <th>Имя пользователя</th>
    <th>ID пользователя</th>
    <th>Был</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($absent as $user) : ?>
    <tr>
      <td><a href='<?php print user_url($user->id); ?>'><?php print $user->name; ?></a></td>
      <td><?php print $user->id; ?></td>
      <td><?php print $user->time ?></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
</body>
</html>
