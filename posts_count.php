<?php
include "simple_html_dom.php";
function get_all_theme_pages($theme_link) {
    $link_list = array();
    $link_list[] = $theme_link;
    $html = file_get_html($theme_link);
    if (!empty($html->find(".linksb .pagelink a"))) {
        if (is_array($links = $html->find(".linksb .pagelink a"))) {
            $link_base = $theme_link;
            $max_page = $links[count($links) - 2]->text();
            for ($i = 2; $i <= $max_page; $i++) {
                $link_list[] = $link_base . '&p=' . $i;
            }
        }
    }
    asort($link_list);
    return $link_list;
}

function get_all_users_by_page($link, array &$users, $start_date = null, $end_date = null) {
    if ($start_date != null or $end_date != null) {
        $check_date = true;
        if ($start_date == false) {
            $start_date = 0;
        }
        if ($end_date == false) {
            $end_date = time() + 24*60*60;
        }
    } else {
        $check_date = false;
    }
    $link = preg_replace("/amp;/", "", $link);
    $html = file_get_html($link);
    $posts = $html->find('.post');
    asort($posts);
    foreach ($posts as $post) {
        if ($check_date) {
            $date_arr = explode(' ', $post->find('a.permalink', 0)->text());
            $date = $date_arr[0];
            $date = (strpos($date, 'Сегодня') !== false) ? time() : $date;
            $date = (strpos($date, 'Вчера') !== false) ? (time() - 24 * 60 * 60) : $date;
            if (!is_numeric($date)) {
                $date = strtotime($date);
            }
            if (($start_date) > $date or $date > ($end_date)) {
                continue;
            }
        }
        $user_name = $post->find('.pa-author',0)->text();
        if (empty($users[$user_name])) {
            $users[$user_name] = 1;
        } else {
            $users[$user_name]++;
        }
    }
    return true;
}

?>

<html>
<head>
    <title>Количество сообщений пользователя в теме</title>
<meta charset="UTF-8" />
    <link rel="stylesheet" href="posts_count.css" />
    <link rel="stylesheet" href="datepicker/themes/classic.css" />
    <link rel="stylesheet" href="datepicker/themes/classic.date.css" />
</head>
<body>
<div class="wrapper">
    <h3>Количество сообщений пользователей в теме</h3>
<form method="POST">
    <div class="row-elem"><label><span>Адрес темы:</span> <input type="text" name="link" /></label></div>
    <div class="row-elem"><label><span>Дата начала:</span> <input type="text" class="datepicker" name="start_date" /></label></div>
    <div class="row-elem"><label><span>Дата окончания:</span> <input type="text" class="datepicker" name="end_date" /></label></div>
    <div class="row-elem"><input type="submit" class="btn btn-large btn-block" value="Подсчитать" /></div>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="datepicker/picker.js"></script>
    <script src="datepicker/picker.date.js"></script>
    <script>
        $('.datepicker').pickadate({
            monthsFull: [ 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthsShort: [ 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'],
            weekdaysFull: [ 'воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота' ],
            weekdaysShort: [ 'вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб' ],
//            showMonthsShort: true,
            today: 'сегодня',
            clear: 'удалить',
            close: 'закрыть',
            firstDay: 1,
            format: 'dd.mm.yyyy', //'d mmm yyyy г.',
            formatSubmit: 'yyyy/mm/dd',
        });
    </script>
</form>
</div>

<?php
if (!empty($_POST['link'])) {
    print '<div class="wrapper">
<table class="table table-hover">
<thead>
<tr><th>№</th><th>Автор</th><th>Количество сообщений</th></tr>
</thead>';
    $start_date = (!empty($_POST['start_date']) ? strtotime("midnight", strtotime($_POST['start_date'])) : null);
    $end_date = (!empty($_POST['end_date']) ? strtotime("tomorrow", strtotime($_POST['end_date'])) - 1 : null);
    $users = array();
    $pages = get_all_theme_pages($_POST['link']);
    foreach ($pages as $page) {
        if (!get_all_users_by_page($page, $users, $start_date, $end_date)) {
            break;
        }
    }
    arsort($users);
  $total = 0;
    $i = 1;
    foreach ($users as $name => $number) {
      $total += $number;
        print '<tr><td>' . $i . '</td><td>' . mb_substr($name, 13) . '</td><td>' . $number . '</td></tr>';
        $i++;
    }
    print '</table></div>';
    print '<div class="wrapper"><b>Всего: </b>'. $total .'</div>';
}

?>

<div class="wrapper">
    <h4>Справка</h4>
    <ul>
        <li>Инструмент подсчитывает количество сообщений каждого пользователя в теме.</li>
        <li>Работает только для тем, открытых для гостей.</li>
        <li>Поддерживая платформа - mybb и партнеры.</li>
        <li>Вы можете указать начальную и/или конечную даты, чтобы подсчитать количество постов за определенный период.
        Если даты не указаны, будут подсчитаны все посты в теме.</li>
        <li><em>Важно!</em> Указывайте ссыку на тему без нормера страницы (#p...)</li>
    </ul>
</div>
</body>
</html>