<?php
include "simple_html_dom.php";
include "config.php";
include "common.php";

function get_all_episodes($forum_url) {
    $forum_url = preg_replace("/amp;/", "", $forum_url);
    $html = file_get_html($forum_url);
    $episode_urls = array();
    $forum = $html->find(".forum", 0);
    foreach ($forum->find(".tclcon") as $left) {
        $link = $left->find("a",0);
        if ($link->text() != "~ шаблон оформления эпизода") {
            $episode_urls[] = $link->href;
        }
    }
    $html->clear();
    return $episode_urls;
}

function get_indexed_episodes_for($forum_key) {
    $links = array();
    $link = db_open();
    $query = "SELECT link FROM episodes WHERE forum = '$forum_key' AND status = 1";
    $result = mysql_query($query) or die("Query failed : " . mysql_error());
    if (mysql_num_rows($result)) {
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
            $links[] = $line["link"];
        }
    }
    db_close($link);
    return $links;
}

function determine_links_for_check() {
    global $forums;
    $need_check = new stdClass();
    foreach ($forums as $key => $forum) {
        $pages = get_all_pages($forum);
        $new_indexed = array();
        foreach ($pages as $page) {
            $new_indexed = array_merge($new_indexed, get_all_episodes($page));
        }
      //  $old_indexed = get_indexed_episodes_for($key);
        $old_indexed = array();
        $new = array_diff($new_indexed, $old_indexed);
        $need_check->new[$key] = $new;
        $moved = array_diff($old_indexed, $new_indexed);
        $need_check->moved[$key] = $moved;
    }
    return $need_check;
}

function add_new_episode($episode) {
    $link = db_open();
    $query = "INSERT INTO episodes (link, title, players, created, summary, forum, status)
              VALUES ('".$episode->link."', '".addslashes($episode->title)
        ."', '".addslashes($episode->players)
        ."', ".addslashes($episode->created)
        .", '".addslashes($episode->summary)
        ."', '".$episode->forum."', 1)";
    mysql_query($query) or die("Query failed : " . mysql_error());
    db_close($link);
}

function update_status($eplink, $status) {
    $link = db_open();
    $query = "UPDATE episodes SET status = $status WHERE link = '$eplink'";
    mysql_query($query) or die("Query failed : " . mysql_error());
    db_close($link);
}

function get_episode_info($link, $forum) {
    $html = file_get_html($link);
    $episode_info = new stdClass();
    $episode_info->link = $link;
    $episode_info->forum = $forum;
    $post = $html->find('.topicpost', 0);
    $date_string = $post->find('a.permalink', 0)->text();
    date_default_timezone_set('Europe/London');
    $date_array = explode(" ", $date_string);
    if ($date_array[0] == "Сегодня") {
        $created = strtotime("midnight today");
    } elseif ($date_array[0] == "Вчера") {
        $created = strtotime("midnight yesterday");
    } else {
        $created = strtotime($date_array[0]."T00:00:00");
    }
    $episode_info->created = $created;
    $content = $post->find('.post-content', 0);
    $episode_info->title = $content->find('p',0)->find('span',0)->find('span',0)->text();
    $episode_info->players = $content->find('p',0)->find('span',0)->find('span',2)->text();
    if ($content->find('blockquote', 1)) {
        $episode_info->summary = $content->find('blockquote', 1)->text();
    } elseif ($content->find('blockquote', 0)) {
        $episode_info->summary = $content->find('blockquote', 0)->text();
    }  else {
        $episode_info->summary = 'Не удалось найти описание.';
    }
    return $episode_info;
}


function archive_links() {
    global $completed_episodes;
    global $abandoned_episodes;
    return array(
        2 => $completed_episodes,
        3 => $abandoned_episodes
    );
}

function get_all_archived_indexed_episodes($status) {
    $links = array();
    $link = db_open();
    $query = "SELECT link FROM episodes WHERE status = $status";
    $result = mysql_query($query) or die("Query failed : " . mysql_error());
    if (mysql_num_rows($result)) {
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
            $links[] = $line["link"];
        }
    }
    db_close($link);
    return $links;
}

function all_forum_episodes($link) {
    $links = get_all_pages($link);
    $episodes = array();
    foreach ($links as $l) {
        $tmp = get_all_episodes($l);
        $episodes = array_merge($episodes, $tmp);
    }
    return $episodes;
}

function get_status($eplink) {
    $link = db_open();
    $query = "SELECT status FROM episodes WHERE link = '$eplink'";
    $result = mysql_query($query) or die("Query failed : " . mysql_error());
    db_close($link);
    if (mysql_num_rows($result)) {
        $row = mysql_fetch_array($result);
        return $row['status'];
    } else {
        return false;
    }
}

function check_archived() {
    $archives = archive_links();
    foreach ($archives as $status => $archive) {
        $new_episodes = all_forum_episodes($archive);
        $old_episodes = get_all_archived_indexed_episodes($status);
        foreach ($new_episodes as $link) {
            if (!in_array($link, $old_episodes)) {
                $epstatus = get_status($link);
                if ($epstatus === false) {
                    $episode_info = get_episode_info($link, 'архив');
                    add_new_episode($episode_info);
                } else {
                    update_status($link, $status);
                }
            }
        }
    }
}

function parse() {
    check_archived();
    $need_check = determine_links_for_check();
    foreach ($need_check->new as $forum => $links) {
        foreach ($links as $link) {
            $episode_info = get_episode_info($link, $forum);
            add_new_episode($episode_info);
        }
    }
}

header('Content-Type: text/html; charset=utf-8');
//parse();
print_r(determine_links_for_check());