<?php
include "simple_html_dom.php";
include "config.php";
include "common.php";

function get_episodes($forum_key) {
    $link = db_open();
    $query = "SELECT * FROM episodes WHERE forum = '$forum_key' ORDER BY created";
    $result = mysql_query($query) or die("Query failed : " . mysql_error());
    $episodes = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $episodes[] = $line;
    }
    db_close($link);
    return $episodes;
}

$forum_keys = array_keys($forums);
header('Access-Control-Allow-Origin: http://yellowcross.rusff.ru');
?>

<style>
  h1 {
    text-align: center;
    font-size: 1.2em!important;
    font-weight: bold!important;
    text-transform: capitalize;
  }
  #user-stats {
    padding: 15px;
  }

  #user-stats table {
    margin-bottom: 15px;
  }

  #user-stats table thead {
    background: rgba(105, 134, 133, 0.4);
    font-size: 1.2em;
  }

  #user-stats table caption {
    background: rgba(105, 134, 133, 0.86);
    font-size: 1.2em;
    font-weight: bold;
    margin-left: 2px;
    margin-right: 2px;
    padding: 3px;
  }

  #user-stats table td {
    text-align: center;
  }
  .note {
    margin-top: 20px;
    font-size: 0.8em;
  }

  .summary-show {
    cursor: pointer;
  }
  .summary-hide {
    display: none;
    white-space: pre-line;
    position: absolute;
    background-color: #9db1b8;
    padding: 10px;
    width: 30%;
  }
  .summary-show:hover+.summary-hide {
    display: block;
  }

  .forum {
    font-weight: bold;
    text-transform: capitalize;
    font-size: 1.2em;
  }
  blockquote {
    display: none;
  }
  blockquote.visible {
    display: block;
  }
  .spoiler-header {
    padding: 10px;
    cursor: pointer;
  }
  .spoiler-box {
    margin-bottom: 10px;
  }
</style>


<?php foreach ($forum_keys as $key): ?>
<?php $episodes = get_episodes($key); ?>
  <div class="quote-box spoiler-box">
    <div class="spoiler-header" onclick="$(this).toggleClass('visible'); $(this).next().toggleClass('visible');">
      <span class="forum"><?php print $key; ?></span> (<?php print count($episodes); ?> эп.)
      </div>
    <blockquote>
      <table>
        <thead>
        <tr>
          <th>Название эпизода</th>
          <th>Участники</th>
          <th>Создан</th>
          <th>Краткое содержание</th>
          <th>Статус</th>
        </tr>
        </thead>
  <?php foreach ($episodes as $episode): ?>
    <tr>
        <td><a href="<?php print $episode["link"]; ?>"><?php print $episode["title"]; ?></a></td>
        <td><?php print $episode["players"]; ?></td>
        <td><?php print date("d.m.Y", $episode["created"]); ?></td>
        <td><span class="summary-show">...</span><div class="summary-hide"><?php print $episode["summary"]; ?></div></td>
        <td><?php switch($episode["status"]) {
            case 1:
              print "активен";
              break;
            case 2:
              print "завершен";
              break;
            case 3:
              print "не закончен";
              break;
          } ?></td>
     </tr>
    <?php endforeach; ?>
      </table>
    </blockquote>
    </div>
    <?php endforeach; ?>


<div class="note">Статистика обновляется раз в сутки.</div>