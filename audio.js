$(document).ready(function() {
    $('.post .post-content').each(function() {
        if ($(this).html().indexOf('[audio]') != -1) {
            var html = $(this).html();
            html.replace('[audio]', '<audio src="');
            html.replace('[/audio]', '" controls></audio>');
        }
    })
})
