<?php
include "simple_html_dom.php";
function get_all_theme_pages($theme_link) {
    $link_list = array();
    $link_list[] = $theme_link;
    $html = file_get_html($theme_link);
    if (is_array($links = $html->find(".linksb .pagelink a"))) {
        foreach ($links as $link) {
            if (strpos($link->class, 'next') === false) {
                $link_list[] = $link->href;
            }
        }
    }
    asort($link_list);
    return $link_list;
}

function get_all_users_by_page($link, array &$users) {
    $link = preg_replace("/amp;/", "", $link);
    $html = file_get_html($link);
    $posts = $html->find('.post');
    asort($posts);
    foreach ($posts as $post) {
        $date = $post->find('a.permalink',0)->text();
        if (strpos($date, 'Сегодня') === false and strpos($date, 'Вчера') === false) {
            if (strtotime($date) <= strtotime("midnight -7 days")) {
                exit;
            }
        }
        $user_name = $post->find('.pa-author a',0)->text();
        if (empty($users[$user_name])) {
            $users[$user_name] = 1;
        } else {
            $users[$user_name]++;
        }
    }
}

function get_all_themes_from_forum($forum_link) {
    $html = file_get_html($forum_link);
    $links = array();
    $forum = $html->find('.forum', 0);
    print($forum);
//    foreach ($forum->find('tr') as $tr) {
//        $tcr = $tr->find('.tcr', 0);
//        print_r($tcr);
//        $date = $tcr->find('a',0)->text();
//        if (strpos($date, 'Сегодня') !== false or strpos($date, 'Вчера') !== false) {
//            $links = $tr->find('.tcl a', 0)->href;
//        } elseif (strtotime($date) >= strtotime("midnight -7 days")) {
//            $links = $tr->find('.tcl a', 0)->href;
//        }
//    }
    return $links;
}

//$users = array();
//$pages = get_all_theme_pages('http://yellowcross.rusff.ru/viewtopic.php?id=606');
//foreach ($pages as $page) {
//    get_all_users_by_page($page, $users);
//}
$links = get_all_themes_from_forum('http://yellowcross.rusff.ru/viewforum.php?id=17');
print_r($links);
