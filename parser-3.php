<?php
include_once "simple_html_dom.php";
include_once "config.php";
include_once "common.php";

function get_char_sheets_per_page($page_link) {
  $page_link = preg_replace("/amp;/", "", $page_link);
  $html = file_get_html($page_link);
  $char_sheets = array();
  $forum = $html->find(".forum", 0);
  foreach ($forum->find(".tclcon") as $left) {
    $row = $left->find("a", 0);
    $title = $row->text();
    $link = $row->href;
    $arr = preg_split("/(\{|\})/", $title);
      $i = 1;
      $fandoms = array();
      while(!empty($arr[$i])) {
          $fandoms[] = $arr[$i];
          $i += 2;
      }
      $char_sheets[$link] = array(
          'fandom' => $fandoms,
          'link' => $link
      );

  }
  $html->clear();
  return $char_sheets;
}

function get_all_char_sheets() {
  global $character_sheets_forum;
  $pages = get_all_pages($character_sheets_forum);
  $char_sheets = array();
  foreach ($pages as $page) {
    $char_sheets = array_merge($char_sheets, get_char_sheets_per_page($page));
  }
  return $char_sheets;
}

function get_all_indexed_char_sheets() {
  $link = db_open();
  $query = 'SELECT `charsheet` FROM `players`';
  $result = mysql_query($query) or die("Query failed : " . mysql_error());
  $players = array();
  while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
    $players[$line['charsheet']] = $line['charsheet'];
  }
  db_close($link);
  return $players;
}

function add_new_char_sheet($char_sheet) {
  $link = db_open();
    $name = addslashes($char_sheet['name']);
    foreach ($char_sheet['fandom'] as $fandom) {
        $fandom = addslashes($fandom);
        $query = "INSERT INTO `players` (`charsheet`, `name`, `id`, `fandom`)
              VALUES ('$char_sheet[link]', '$name', $char_sheet[id], '$fandom')";
        $result = mysql_query($query) or die("Query failed : " . mysql_error());
    }

  db_close($link);
}

function char_sheet_author_info($char_sheet) {
  $url = preg_replace("/amp;/", "", $char_sheet['link']);
  $html = file_get_html($url);
  $post = $html->find('.topicpost', 0);
    $author = $post->find(".pa-author a", 0);
    $author_name = $author->innertext;
    $author_url = $author->href;
    $author_url_array = explode('=', $author_url);
    $author_id = $author_url_array[1];
  $char_sheet['name'] = $author_name;
  $char_sheet['id'] = $author_id;
  return $char_sheet;
}

function delete_char_sheet($char_link) {
  $link = db_open();
  $query = "DELETE FROM `players` WHERE `charsheet` = '$char_link'";
  $result = mysql_query($query) or die("Query failed : " . mysql_error());
  db_close($link);
}

function parse_char_sheets() {
  $forum_char_sheets = get_all_char_sheets();
  $new_links = array_keys($forum_char_sheets);
  $indexed_links = get_all_indexed_char_sheets();

//  foreach ($new_links as $link) {
//    if (!in_array($link, $indexed_links)) {
//      $full_record = char_sheet_author_info($forum_char_sheets[$link]);
//      add_new_char_sheet($full_record);
//      unset($indexed_links[$link]);
//    }
//  }
    $i = 1;
  foreach ($indexed_links as $link) {
      if (!in_array($link, $new_links)){
      print $i;
          print $link;
   //   delete_char_sheet($link);
      $i++;
      print '<br />';
  }
  }
}

parse_char_sheets();