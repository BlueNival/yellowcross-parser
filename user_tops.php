<?php
include "simple_html_dom.php";
include "config.php";
include "common.php";

function get_top_N_by_posts_for_time($n, $time) {
    $link = db_open();
    $query = "SELECT authorname, authorid, count(*) as posts FROM posts WHERE created > $time GROUP BY authorid ORDER BY posts DESC LIMIT $n";
    $result = mysql_query($query) or die("Query failed : " . mysql_error());
    $posts = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $posts[] = $line;
    }
    db_close($link);
    return $posts;
}

function user_pages() {
  $pages = array();
  $pages[] = FORUM."/userlist.php";
  $html = file_get_html($pages[0]);
  if (is_array($links = $html->find(".linksb .pagelink a"))) {
    foreach ($links as $link) {
      if (strpos($link->class, 'next') === false) {
        $pages[] = FORUM."/userlist.php?p=".$link->plaintext;
      }
    }
  }
  return $pages;
}

function get_all_users() {
  $pages = user_pages();
  $users = array();
  foreach ($pages as $page) {
    $html = file_get_html($page);
    foreach ($html->find(".usertable tr") as $tr) {
      if ($tr->find(".tc3", 3)->plaintext == "Был") {
        continue;
      }
      $link = $tr->find(".usersname a", 0);
      $time = trim($tr->find(".tc3",3)->plaintext);
      if ($time == "Сегодня") {
        $time = date("Y-m-d", time());
      }
      if ($time == "Вчера") {
        $time = date("Y-m-d", strtotime("Yesterday"));
      }
      $tmp = new stdClass();
      $link_href_array = explode("=", $link->href);
      $tmp->name = $link->plaintext;
      $tmp->id = $link_href_array[1];
      $tmp->time = $time;
      $users[] = $tmp;
    }
  }
  return $users;
}

function absent_for_N_days($n, $users) {
  $absent = array();
  $critical_point = strtotime("midnight -$n days");
  foreach ($users as $user) {
    $time = strtotime($user->time." 00:00:00");
    if ($time < $critical_point) {
      $absent[] = $user;
    }
  }
  return $absent;
}

function no_posts_for_N_days($n, $users) {
  $link = db_open();
  $date = strtotime("midnight -$n days");
  $query = "CREATE TEMPORARY TABLE users(id int, name varchar(255));";
  mysql_query($query) or die("Query failed : " . mysql_error());
  foreach ($users as $user) {
    $query = "INSERT INTO users values(".$user->id.", '".addslashes($user->name)."');";
    mysql_query($query) or die("Query failed : " . mysql_error());
  }
  $query = "SELECT u.id AS id, u.name AS name, COUNT(postlink) AS posts FROM users AS u LEFT JOIN posts AS p ON p.authorid = u.id WHERE p.created > $date OR ISNULL(p.created) GROUP BY id;";
  $result = mysql_query($query) or die("Query failed : " . mysql_error());
  $loosers = array();
  while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
    if ($line["posts"] == 0  and $line["id"] != 2 and $line["id"] != 8) {
      $loosers[] = $line;
    }
  }
    db_close($link);
    return $loosers;
}

date_default_timezone_set('Europe/London');
if (date("N", time()) == 1) {
  $time = strtotime("midnight today");
} elseif (date("N", time()) == 7) {
  $time = strtotime("midnight monday last week");
}
else {
  $time = strtotime("midnight monday this week");
}
$top_week = get_top_N_by_posts_for_time(10, $time);
$time = strtotime("midnight first day of this month");
$top_month = get_top_N_by_posts_for_time(10, $time);
$users = get_all_users();
$absent = absent_for_N_days(14, $users);
$loosers = no_posts_for_N_days(30, $users);
header('Access-Control-Allow-Origin: http://yellowcross.rusff.ru');
?>

<style>
  h1 {
    text-align: center;
    font-size: 1.2em!important;
    font-weight: bold!important;
    text-transform: capitalize;
  }
  #user-stats {
    padding: 15px;
  }

  #user-stats table {
    margin-bottom: 15px;
  }

  #user-stats table thead {
    background: rgba(105, 134, 133, 0.4);
    font-size: 1.2em;
  }

  #user-stats table caption {
    background: rgba(105, 134, 133, 0.86);
    font-size: 1.2em;
    font-weight: bold;
    margin-left: 2px;
    margin-right: 2px;
    padding: 3px;
  }

  #user-stats table td {
    text-align: center;
  }
  .note {
    margin-top: 20px;
    font-size: 0.8em;
  }
</style>
<table>
    <caption>Лучшие за неделю</caption>
    <thead>
    <tr>
        <th>Имя пользователя</th>
        <th>ID пользователя</th>
        <th>Количество постов</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($top_week as $row) : ?>
        <tr>
            <td><a href='<?php print user_url($row["authorid"]); ?>'><?php print $row["authorname"]; ?></a></td>
            <td><?php print $row["authorid"]; ?></td>
            <td><?php print $row["posts"] ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<table>
    <caption>Лучшие за месяц</caption>
    <thead>
    <tr>
        <th>Имя пользователя</th>
        <th>ID пользователя</th>
        <th>Количество постов</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($top_month as $row) : ?>
        <tr>
            <td><a href='<?php print user_url($row["authorid"]); ?>'><?php print $row["authorname"]; ?></a></td>
            <td><?php print $row["authorid"]; ?></td>
            <td><?php print $row["posts"] ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<table>
  <caption>Отсутствуют больше 14 дней</caption>
  <thead>
  <tr>
    <th>Имя пользователя</th>
    <th>ID пользователя</th>
    <th>Был</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($absent as $user) : ?>
    <tr>
      <td><a href='<?php print user_url($user->id); ?>'><?php print $user->name; ?></a></td>
      <td><?php print $user->id; ?></td>
      <td><?php print $user->time ?></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

<table>
  <caption>Нет постов за последние 30 дней</caption>
  <thead>
  <tr>
    <th>Имя пользователя</th>
    <th>ID пользователя</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach ($loosers as $row) : ?>
    <tr>
      <td><a href='<?php print user_url($row["id"]); ?>'><?php print $row["name"]; ?></a></td>
      <td><?php print $row["id"]; ?></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

<div class="note">Статистика обновляется раз в час.</div>
