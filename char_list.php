<?php
include "config.php";
include "common.php";
function get_all_players() {
    $link = db_open();
    $query = 'SELECT * FROM `players` ORDER BY `fandom`, `name`';
    $result = mysql_query($query) or die("Query failed : " . mysql_error());
    $players = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $players[$line['fandom']][] = array(
            'id' => $line['id'],
            'name' => $line['name'],
            'charsheet' => $line['charsheet']
        );
    }
    db_close($link);
    return $players;

}
?>

<?php header('Access-Control-Allow-Origin: http://yellowcross.rusff.ru'); ?>
<style>
    @import url(http://fonts.googleapis.com/css?family=Dancing+Script);
    .letter {
        font-family: 'Dancing Script', cursive;
        font-size: 3em;
        text-align: center;
        margin-top: 20px;
    }
    h1 {
        text-align: center;
        font-size: 1.2em!important;
        font-weight: bold!important;
        text-transform: capitalize;
    }
    #user-stats {
        margin: 20px;
    }
    .spoiler-box {
        padding: 10px;
        border-radius: 4px;
    }
    .spoiler-header {
        padding: 1px 10px;
        cursor: pointer;
    }
    .fandom {
        font-weight: bold;
        font-family: 'Dancing Script', cursive;
        font-size: 1.5em;
    }
    .char-number {
        float: right;
        margin-top: 4px;
    }
    blockquote {
        display: none;
    }
    blockquote.visible {
        display: block;
        border-top: 2px rgba(0, 95, 100, 0.30) solid;
    }
    .icon-set {
        float: right;
        margin-top: -11px;
    }
    .hidden {
        display: none;
        position: absolute;
        margin-left: -500px;
        width: 500px;
        background: #9FB3BC;
        border: 1px #176578 solid;
        border-radius: 4px;
        box-shadow: 3px 3px 3px rgba(23, 101, 120, 0.27);
    }
    .hidden table {
        margin-top: -20px;
    }
    .hidden td {
        vertical-align: top;
    }
    .hidden table img {
        width: 180px;
        border: 1px #176578 solid;
    }
    .hidden table {
        table-layout: fixed;
        width: 500px;
    }
    .hidden table tr td:first-child {
        width: 182px;
    }
    .load {
        padding: 10px;
    }
    .icon {
        display: inline-block;
        width: 24px;
        height: 24px;
        margin-left: 5px;
        opacity: 0.7
    }
    .icon:hover {
        opacity: 1
    }
    .icon.preview {
        background: url('http://sh.uploads.ru/BMamL.png');
    }
    .icon.charsheet {
        background: url('http://sh.uploads.ru/nwBVg.png');
    }
    .icon.profile {
        background: url('http://sh.uploads.ru/HaVdD.png');
    }
    .icon.chronology {
        background: url('http://sh.uploads.ru/m7Lkq.png')
    }
    #user-stats li {
        margin-left: 40px!important;
        clear: both;
        height: 14px;
        margin-top: 5px!important;
        border-bottom: 1px rgba(86, 121, 117, 0.36) solid;
        padding: 10px 10px 0!important;
        list-style: inherit!important;
    }
    #user-stats li:before {
        content: url('http://sg.uploads.ru/inLjO.png');
        position: absolute;
        margin-left: -45px;
        margin-top: -8px;
    }
    span[style*="font-family: Garamond"] {
        font-size: 1.5em;
        padding: 10px 0;
        display: block;
    }
    .hidden hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(88, 115, 119, 0), rgba(41, 113, 119, 1), rgba(88, 115, 119, 0));
    }
    .spoiler-box {
        margin-top: 1px!important;
    }
    .note {
        margin-top: 20px;
        font-size: 0.8em;
    }
</style>

<?php
$players = get_all_players();
$fandoms = array_keys($players);
$arr = range('A', 'Z'); ?>

<?php foreach ($arr as $letter): ?>
<div class="letter"><?php print $letter; ?></div>
  <?php foreach ($fandoms as &$fandom): ?>
       <? if (strpos(strtoupper($fandom), $letter) === 0): ?>

<div class="quote-box spoiler-box">
    <div class="spoiler-header" onclick="$(this).toggleClass('visible'); $(this).next().toggleClass('visible');">
        <span class="fandom"><?php print $fandom; ?></span> <span class="char-number">(<?php print count($players[$fandom]) ?> перс.)</span>
    </div>
    <blockquote>
        <ul>
            <?php foreach ($players[$fandom] as $player): ?>
            <li><?php print $player['name']; ?>
                <div class="icon-set">
                    <span class="preview icon" data-charsheet="<?php print $player['charsheet']; ?>"></span>
                    <div class="hidden"></div>
                    <a class="charsheet icon" title="Анкета" href="<?php print $player['charsheet']; ?>"></a>
                    <a class="profile icon" title="Профиль" href="/profile.php?id=<?php print $player['id']; ?>"></a>
                    <a class="chronology icon" title="Хронология" href="/pages/hronologiya?id=<?php print $player['id']; ?>"></a>
                </div>
            </li>
          <?php endforeach; ?>
        </ul>
    </blockquote>
</div>

<?php endif; ?>
<?php endforeach; ?>
<?php endforeach; ?>

<div class="note">Статистика обновляется раз в шесть часов.</div>