<?php
include "simple_html_dom.php";
include "config.php";
include "common.php";

function user_pages() {
    $pages = array();
    $pages[] = FORUM."/userlist.php";
    $html = file_get_html($pages[0]);
    if (is_array($links = $html->find(".linksb .pagelink a"))) {
        foreach ($links as $link) {
            if (strpos($link->class, 'next') === false) {
                $pages[] = FORUM."/userlist.php?p=".$link->plaintext;
            }
        }
    }
    return $pages;
}

function get_user_data($id)
{
    $profile_url = user_url($id);
    $html = file_get_html($profile_url);
    $profile = new stdClass();
    $profile->name = $html->find("#profile-name", 0)->plaintext;
    $profile->avatar = $html->find("#profile-left img", 0)->src;
    return $profile;
}

function get_all_users() {
    $pages = user_pages();
    $users = array();
    foreach ($pages as $page) {
        $html = file_get_html($page);
        foreach ($html->find(".usertable tr") as $tr) {
            if ($tr->find(".tc3", 3)->plaintext == "Был") {
                continue;
            }
            $link = $tr->find(".usersname a", 0);
            $time = trim($tr->find(".tc3",3)->plaintext);
            if ($time == "Сегодня") {
                $time = date("Y-m-d", time());
            }
            if ($time == "Вчера") {
                $time = date("Y-m-d", strtotime("Yesterday"));
            }
            $tmp = new stdClass();
            $link_href_array = explode("=", $link->href);
            $tmp->name = $link->plaintext;
            $tmp->id = $link_href_array[1];
            $tmp->time = $time;
            $users[] = $tmp;
        }
    }
    return $users;
}

$tmp_users = get_all_users();
$users = array();
foreach ($tmp_users as $user) {
    $users[] = get_user_data($user->id);
}
$new_users = $users;
shuffle($new_users);
foreach ($users as $i => $user) {
    print  "masks['$user->name'] = {name:'".$new_users[$i]->name."', avatar:'".$new_users[$i]->avatar."'}; <br />";
}