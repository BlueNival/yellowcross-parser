<?php
include "simple_html_dom.php";
include "config.php";
include "common.php";
header('Access-Control-Allow-Origin: http://yellowcross.rusff.ru');
$id = $_GET["id"];

function get_all_user_posts_of_last_N_days($number = null, $id)
{
    $link = db_open();
  if (!$number) {
    $time = 0;
  }
  else {
    $time = strtotime("midnight -$number days");
  }
    $query = "SELECT * FROM posts WHERE created > $time AND authorid = $id ORDER BY created DESC";
    $result = mysql_query($query) or die("Query failed : " . mysql_error());
    $posts = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $posts[] = $line;
    }
    db_close($link);
    return $posts;
}

function get_user_data($id)
{
    $profile_url = user_url($id);
    $html = file_get_html($profile_url);
    $profile = new stdClass();
    $profile->name = $html->find("#profile-name", 0)->plaintext;
    $profile->stats = $html->find("#profile-title", 0)->plaintext;
    $profile->avatar = $html->find("#profile-left img", 0)->src;
    return $profile;
}

function count_horns($chars) {
  if ($chars < 3000) return 25;

  if ($chars < 10000) {
    if ($chars % 1000 < 500) {
      return ((int) ($chars / 1000)) * 10;
    }
    else {
      return ((int) ($chars / 1000)) * 10 + 5;
    }
  } else {
    return (105 + ((int)(($chars - 10000) / 500)) * 10);
  }
}

$posts = get_all_user_posts_of_last_N_days(null, $id);
$user = get_user_data($id);
?>

<style>

</style>
<div id="left-user-stats-col">
    <h3><?php print $user->name; ?></h3>
    <span><?php print $user->stats; ?></span>
    <img src="<?php print FORUM . "/" . $user->avatar; ?>">
</div>
<div id="right-user-stats-col">
    <?php if (!count($posts)) { ?>
        Постов пока нет.
    <?php } else { ?>
      <table>
        <thead>
        <tr>
          <th>Время</th>
          <th>Пост</th>
          <th>Раздел</th>
          <th>Знаков</th>
          <th>Рожек</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $html = NULL;
        $date = date("d.m.Y", $posts[0]["created"]);
        $total_horns = 0;
        foreach ($posts as $post) {
          if (date("d.m.Y", $post["created"]) != $date) {
            $html .= '<tr class="day-header"><td colspan="5"><span class="stats-date">' . $date . '</span> <span class="day-horns">(Заработано рожек: ' . $total_horns . ')</span></td></tr>' . $tmp;
            $date = date("d.m.Y", $post["created"]);
            $total_horns = 0;
            $tmp = null;
          }
            $tmp .= '<tr>
          <td>' . date("H:i:s", (int) $post["created"]) . '</td>
          <td><a href="' . $post["postlink"] . '" target="blank">' . $post["themename"] . '</a></td>
          <td>' . $post["forum"] . '</td>';
            if (!empty($post["totalchars"])) {
              $tmp .= '<td>' . $post["totalchars"] . '</td>';
              $horns = count_horns($post["totalchars"]);
              $total_horns += $horns;
            }
            else {
              $horns = NULL;
            }
            $tmp .= '<td>' . $horns . '</td></tr>';

        }
        $html .= '<tr class="day-header"><td colspan="5"><span class="stats-date">' . $date . '</span> <span class="day-horns">(Заработано рожек: ' . $total_horns . ')</span></td></tr>' . $tmp;
        print $html;
      ?>
        </tbody>
      </table>

      <hr>
        <div id="posts-sum">Всего постов: <?php print count($posts); ?></div>
    <?php } ?>
  <div class="note">Статистика обновляется раз в час.</div>
</div>
