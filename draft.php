<?php

function all_today_posts_table() {
global $forums;
$posts = array();
$indexed = get_indexed_today_posts();
foreach ($forums as $forum) {
$episodes = get_all_episodes($forum, $indexed);
foreach ($episodes as $episode) {
$posts = array_merge($posts, get_new_today_posts($episode, $indexed));
}
}
$html = "<table>
  <caption>Посты за сегодня</caption>
  <thead>
  <tr>
    <th>Имя пользователя</th>
    <th>Ссылка на пост</th>
    <th>Дата</th>
  </tr>
  </thead><tbody>";
  foreach ($posts as $post) {
  $html .= "<tr><td><a href='".user_url($post->author_id)."'>$post->author_name</a></td>
    <td><a href='$post->url'>$post->name</a></td>
    <td>$post->time</td>";
    }
    $html .= "</tbody></table>";
return $html;
} ?>

<html>
<head>
  <meta charset="UTF-8">
</head>
<body>
<?php echo all_today_posts_table(); ?>
</body>
</html>

<div id="user-stats"></div>
<script type="text/javascript">
    $(function(){
        $.ajax({
            url: 'http://iskra-trpg.ru/ycross/user_tops.php',
            success: function(data){
                $('#user-stats').html(data);
            },
            error: function(data){
                console.log(data);
            },
            type: "GET",
            dataType: "text"
        });

    });
</script>