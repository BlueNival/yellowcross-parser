<?php
include "simple_html_dom.php";
include "config.php";
include "common.php";
header('Content-Type: text/html; charset=utf-8');

function get_all_episodes($forum_url, $indexed, $day_mark = 'Сегодня')
{
  $forum_url = preg_replace("/amp;/", "", $forum_url);
  $html = file_get_html($forum_url);
  $episode_urls = array();
  $forum = $html->find(".forum", 0);
  foreach ($forum->find("tr") as $row) {
    $date = $row->find(".tcr", 0);
    if (strpos($date->plaintext, $day_mark) !== false) {
      if (!in_array($date->find("a",0)->href, $indexed)) {
        $episode_url = $row->find(".tcr a", 0);
        $episode_urls[] = $episode_url->href;
      }
    }
  }
  return $episode_urls;
}

function get_indexed_today_posts($day_mark = 'Сегодня') {
  if ($day_mark == 'Вчера') {
    $time = strtotime("yesterday GMT");
  } else {
    $time = strtotime("today GMT");
  }
  $post_links = array();
  $link = db_open();
  $query = "SELECT postlink FROM posts WHERE created > $time";
  $result = mysql_query($query) or die("Query failed : " . mysql_error());
  while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
    $post_links[] = $line["postlink"];
  }
  db_close($link);
  return $post_links;
}

function add_new_posts_of_forum($posts, $forumname, $day_mark) {
  $link = db_open();

    if ($day_mark == 'Вчера') {
        $day = strtotime("yesterday GMT");
    } else {
        $day = strtotime("today GMT");
    }

  foreach ($posts as $post) {
    $name = addslashes($post->name);
	$authorname = addslashes($post->author_name);
  $created = $day+strtotime("1970-01-01T".$post->time."+00:00");
    $query = "INSERT INTO posts (authorname, authorid, postlink, themename, created, forum, totalchars)
              VALUES ('$authorname', $post->author_id, '$post->url', '$name', $created, '$forumname', $post->totalchars)";
    $result = mysql_query($query) or die("Query failed : " . mysql_error());
 //   echo $query."<br />";
  }
  db_close($link);
}

function get_new_today_posts($episode_url, $indexed, $day_mark = 'Сегодня') {
  $episode_url = preg_replace("/amp;/", "", $episode_url);
  $html = file_get_html($episode_url);
  $title = $html->find('title', 0);
  $name = $title->plaintext;
  $posts = array();
  foreach ($html->find(".post") as $post_body) {
    $post_link = $post_body->find("a.permalink", 0);
    if (!in_array($post_link->href, $indexed)) {
      $post_date = $post_link->plaintext;
      $post_date_array = explode(" ", $post_date);
      if (strpos($post_date, $day_mark) !== FALSE and (strpos($post_body->class, 'topicpost') === false)) {
        $author = $post_body->find(".pa-author a", 0);
        $author_name = $author->innertext;
        $author_url = $author->href;
        $author_url_array = explode('=', $author_url);
        $author_id = $author_url_array[1];
        $post_url = $post_link->href;
          $post_content = $post_body->find(".post-content", 0);
          $new_line = trim(preg_replace('/\s\s+/', '',$post_content->plaintext));
          $new_line =  str_replace(array("\r\n", "\r", "\n"), '', $new_line);
          $totalchars = mb_strlen($new_line, "UTF-8");
        $post = new stdClass();
        $post->author_name = $author_name;
        $post->author_id = $author_id;
        $post->url = $post_url;
        $post->time = $post_date_array[1];
        $post->name = $name;
          $post->totalchars = $totalchars;
        $posts[] = $post;
      }
    }
  }
  return $posts;
}

function parse() {
  global $forums;
  if ((int)date('H', time()) < 1) {
    $day_mark = 'Вчера';
  } else {
    $day_mark = 'Сегодня';
  }
  $indexed = get_indexed_today_posts($day_mark);
  foreach ($forums as $forumname => $forum) {
    $posts = array();
    $episodes = get_all_episodes($forum, $indexed, $day_mark);
    foreach ($episodes as $episode) {
      $posts = array_merge($posts, get_new_today_posts($episode, $indexed, $day_mark));
    }
    add_new_posts_of_forum($posts, $forumname, $day_mark);
  }
}

parse();