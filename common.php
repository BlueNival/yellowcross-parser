<?php
include_once "simple_html_dom.php";
include_once "config.php";

function db_open() {
    global $config;
    $link = mysql_connect($config->db_host, $config->db_user, $config->db_password);
    if (!$link) {
        die('Не удалось соединиться : ' . mysql_error());
    }
    $db_selected = mysql_select_db($config->db_name, $link);
    if (!$db_selected) {
        die ('Не удалось выбрать базу foo: ' . mysql_error());
    }
    mysql_query('SET NAMES utf8');
    return $link;
}

function db_close($link) {
    mysql_close($link);
}

function user_url($id) {
    return FORUM."/profile.php?id=".$id;
}

function get_all_pages($theme_link) {
    $link_list = array();
    $link_list[] = $theme_link;
    $html = file_get_html($theme_link);
    $tmp = $html->find(".linksb .pagelink a");
    if (!empty($tmp)) {
        if (is_array($links = $html->find(".linksb .pagelink a"))) {
            $link_base = $theme_link;
            $max_page = $links[count($links) - 2]->text();
            for ($i = 2; $i <= $max_page; $i++) {
                $link_list[] = $link_base . '&p=' . $i;
            }
        }
    }
    asort($link_list);
    return $link_list;
}
