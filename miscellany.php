<?php
include "simple_html_dom.php";
include "config.php";
include "common.php";

function get_episodes($forum_key) {
    $link = db_open();
    $query = "SELECT * FROM episodes WHERE forum = '$forum_key' ORDER BY created";
    $result = mysql_query($query) or die("Query failed : " . mysql_error());
    $episodes = array();
    while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
        $episodes[] = $line;
    }
    db_close($link);
    return $episodes;
}

$forum_keys = array_keys($forums);
header('Access-Control-Allow-Origin: http://yellowcross.rusff.ru');
?>

<style>
  h1 {
    text-align: center;
    font-size: 1.2em!important;
    font-weight: bold!important;
    text-transform: capitalize;
  }
  #user-stats {
    padding: 15px;
  }

  #user-stats table {
    margin-bottom: 15px;
  }

  #user-stats table thead {
    background: rgba(105, 134, 133, 0.4);
    font-size: 1.2em;
  }

  #user-stats table caption {
    background: rgba(105, 134, 133, 0.86);
    font-size: 1.2em;
    font-weight: bold;
    margin-left: 2px;
    margin-right: 2px;
    padding: 3px;
  }

  #user-stats table td {
    text-align: center;
  }
  .note {
    margin-top: 20px;
    font-size: 0.8em;
  }

  .summary-show {
    cursor: pointer;
  }
  .summary-hide {
    display: none;
    white-space: pre-line;
    position: absolute;
    background-color: #9db1b8;
    padding: 10px;
    width: 30%;
  }
  .summary-show:hover+.summary-hide {
    display: block;
  }

  .forum {
    font-weight: bold;
    text-transform: capitalize;
    font-size: 1.2em;
  }
  blockquote {
    display: none;
  }
  blockquote.visible {
    display: block;
  }
  .spoiler-header {
    padding: 10px;
    cursor: pointer;
  }
  .spoiler-box {
    margin-bottom: 10px;
  }
    .header-forum-stats {
        float: right;
        font-style: italic;
        font-size: 0.9em;
        margin-top: -5px;
    }
    .total {
        margin-top: 15px;
    }
    .total-label {
        font-weight: bold;
        font-style: italic;
    }
    #status-filter-box {
      float: right;
    }
    .clear {
      margin-top: 40px;
    }
  .type-checker {
    display: inline-block;
    cursor: pointer;
    margin-left: 10px !important;
  }
</style>

<div id="status-filter-box">
  <label class="type-checker"><input type="checkbox" class="checker" id="active" checked> Активные</label>
  <label class="type-checker"><input type="checkbox" class="checker" id="closed" checked> Завершенные</label>
  <label class="type-checker"><input type="checkbox" class="checker" id="abandoned" checked> Незаконченные</label>
</div>
<div class="clear"></div>

<?php
$total_active = 0;
$total_closed = 0;
$total_abandoned = 0;
$total = 0;
?>

<?php foreach ($forum_keys as $key): ?>
<?php $episodes = get_episodes($key); ?>

    <?php $html = null;
    $forum_active = 0;
    $forum_closed = 0;
    $forum_abandoned = 0;
    $total += count($episodes);
    foreach ($episodes as $episode) {
        switch($episode["status"]) {
                    case 1:
                        $img = '<img src="http://sh.uploads.ru/2yraN.png" title="Активен" />';
                        $forum_active ++;
                        $total_active++;
                        $row_class = 'class= "active"';
                        break;
                    case 2:
                        $img = '<img src="http://sh.uploads.ru/zqMxG.png" title="Завершен"/>';
                        $forum_closed++;
                        $total_closed++;
                      $row_class = 'class= "closed"';
                        break;
                    case 3:
                        $img = '<img src="http://sg.uploads.ru/eqlLF.png" title="Не закончен"/>';
                        $forum_abandoned++;
                        $total_abandoned++;
                      $row_class = 'class= "abandoned"';
                        break;
                }
      $html .= '<tr '.$row_class.'><td>'.$img;
    $html .= '</td>
            <td>'.date("d.m.Y", $episode["created"]).'</td>
            <td><a href="'.$episode["link"].'">'.$episode["title"].'</a>
            <span class="summary-show">...</span><div class="summary-hide">'.$episode["summary"].'</div></td>
            <td>'.$episode["players"].'</td>
        </tr>';
    } ?>

  <div class="quote-box spoiler-box">
    <div class="spoiler-header" onclick="$(this).toggleClass('visible'); $(this).next().toggleClass('visible');">
      <span class="forum"><?php print $key; ?></span>
        <span class="header-forum-stats">Всего: <?php print count($episodes); ?> эп.
        <br />Активно: <?php print $forum_active; ?>; Закрыто: <?php print $forum_closed; ?>;
            Не закончено: <?php print $forum_abandoned; ?></span>
      </div>
    <blockquote>
      <table>
        <thead>
        <tr>
          <th width="40px"></th>
          <th width="80px">Создан</th>
          <th>Название и краткое содержание</th>
          <th>Игроки</th>
        </tr>
        </thead>
          <?php print $html; ?>
      </table>
    </blockquote>
    </div>
    <?php endforeach; ?>
<div class="total"><p><span class="total-label">Всего эпизодов:</span> <?php print $total; ?></p>
<p> <span class="total-label">Активно:</span> <?php print $total_active; ?>; <span class="total-label">Закрыто:</span> <?php print $total_closed; ?>;
    <span class="total-label">Не закончено:</span> <?php print $total_abandoned; ?></p></div>

<div class="note">Статистика обновляется раз в сутки.</div>
